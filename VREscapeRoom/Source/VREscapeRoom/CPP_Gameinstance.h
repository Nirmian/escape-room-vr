// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CPP_Gameinstance.generated.h"

/**
 * 
 */

UCLASS(config=GameUserSettings)
class VRESCAPEROOM_API UCPP_Gameinstance : public UGameInstance
{
	GENERATED_BODY()

	UPROPERTY(config)
		bool useLeftHandSetting;
	
	UFUNCTION(BlueprintCallable, Category = "CPP|KeyMapping")
		void changeMovementHandF(bool useLeftHand);

	UFUNCTION(BlueprintCallable, Category = "CPP|KeyMapping")
		bool loadHandMovementSetting();
};
