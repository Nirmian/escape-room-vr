// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VRCharacter.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CPP_VRCharacter.generated.h"

UCLASS()
class VRESCAPEROOM_API ACPP_VRCharacter : public AVRCharacter
{
	GENERATED_BODY()
	
	USkeletalMeshComponent* skmMesh;
	bool bPCMode;
	FVector vForwardDirection;
	FVector vRightDirection;

	UFUNCTION()
		void onRotationDelay();

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UCameraComponent* DebugCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		bool bCanRotate = true;

	UFUNCTION(BlueprintCallable)
		static void printSomeText(FString sText);

	UFUNCTION(BlueprintCallable)
		static TArray<FStaticMaterial> getMeshMaterials(UStaticMesh* referencedMesh);

	UFUNCTION(BlueprintCallable, Category = "CPP|Movement")
		void addForwardInput(float value);

	UFUNCTION(BlueprintCallable, Category = "CPP|Movement")
		void addRightInput(float value);

	UFUNCTION(BlueprintCallable, Category = "CPP|Movement")
		void addHorizontalRotation(float value);
	
	ACPP_VRCharacter();

};
