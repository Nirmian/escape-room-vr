// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CPP_GrippableInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCPP_GrippableInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class VRESCAPEROOM_API ICPP_GrippableInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="CPP")
	void getHandPose(FTransform& relativeTransform, UAnimSequence*& pose, USceneComponent*& componentToGrip);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="CPP")
	void getGripObject(UObject*& object, FTransform& relativeTransform, bool& isSlotGrip);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="CPP")
	void toggleHighlight();
};
