# IMR project
Project idea: Escape room
## Team members
- Paraschiv Tudor   (A5)
- Miron Dorin       (A5)
- Ciobanu Sebastian (A6)
- Iacobescu Tudor   (A6)

## Technologies
- engine: Unreal Engine 4
- VR framework: VR Expansion Plugin

## Link for state of the art
https://docs.google.com/document/d/1gR4CqLmTuRasDl3NHHdBzOf-DBv-Mky-18G7e703dMw/edit?usp=sharing